<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
 * Login and register user's routes
 */
Route::auth();

/*
 * User dashboard routes
 */
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('dashboard', 'UserController@home');
    Route::get('dashboard/profile', 'UserController@showProfile');

    Route::get('dashboard/bookreading', 'UserController@showBookreading');
    Route::get('dashboard/bookreading/modify', 'UserController@modifyBookreading');
});

/*
 * Admin dashboard routes
 */
Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin', 'AdminController@index');
    // Registration Routes...
    Route::get('admin/register', 'AdminAuth\AuthController@showRegistrationForm');
    Route::post('admin/register', 'AdminAuth\AuthController@register');
});



Route::group(['middleware' => ['web']], function () {
    //Login Routes...
    Route::get('/admin/login', 'AdminAuth\AuthController@showLoginForm');
    Route::post('/admin/login', 'AdminAuth\AuthController@login');
    Route::get('/admin/logout', 'AdminAuth\AuthController@logout');
});
