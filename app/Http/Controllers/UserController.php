<?php

namespace App\Http\Controllers;

use App\Bookreading;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Book;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(Request $request)
    {
        if (isset($request->action) && $request->action == 'change_menu_status') {

            $user_id = Auth::guard('user')->user()->id;

            if (Auth::guard('user')->user()->dash_menu_status == 'expanded') {

                User::whereId($user_id)->update(['dash_menu_status' => 'folded']);
                $menu_status_change = 'folded';

            } else {

                User::whereId($user_id)->update(['dash_menu_status' => 'expanded']);
                $menu_status_change = 'expanded';

            }

            return redirect('dashboard');
        }
        return view('userdash.dashboard', compact('menu_status_change'));
    }

    public function showProfile()
    {
        return view('userdash.profile');
    }

    public function showBookreading()
    {
        $user = Auth::guard('user')->user();
        $user_books = User::find($user->id)->books()->get();
        $user_bookreadings = User::find($user->id)->bookreadings()->get();

        return view('userdash.bookreading', compact('user_books', 'user_bookreadings'));
    }

    public function modifyBookreading(Request $request)
    {
        if ($request->action == 'add_book') {

            if ($request->book_color == 'custom')
                $bookcolor = 'teal';
            elseif ($request->book_color == 'white')
                $bookcolor = 'white';
            elseif ($request->book_color == 'primary')
                $bookcolor = 'blue';
            elseif ($request->book_color == 'success')
                $bookcolor = 'green';
            elseif ($request->book_color == 'info')
                $bookcolor = 'light-blue';
            elseif ($request->book_color == 'warning')
                $bookcolor = 'orange';
            elseif ($request->book_color == 'danger')
                $bookcolor = 'red';
            elseif ($request->book_color == 'inverse')
                $bookcolor = 'black';
            elseif ($request->book_color == 'purple')
                $bookcolor = 'purple';
            elseif ($request->book_color == 'pink')
                $bookcolor = 'pink';

            $new_book_id = Book::insertGetId([
                'user_id' => Auth::guard('user')->user()->id,
                'book_title' => $request->book_title,
                'book_color' => $bookcolor
            ]);
            if (Book::whereId($new_book_id)->exists()) {
                return ($new_book_id);
            } else {
                return ('note added');
            }
        }

        if ($request->action == 'add_bookreading') {
//            return var_dump($request->$request->book_id);
            $new_bookreading_id = Bookreading::insertGetId([
                'user_id' => Auth::guard('user')->user()->id,
                'book_id' => $request->book_id,
                'day_of_week' => $request->day_of_week,
                'start_hour' => $request->start_hour,
                'start_minute' => $request->start_minute,
                'end_hour' => $request->end_hour,
                'end_minute' => $request->start_minute
            ]);

            if (Bookreading::whereId($new_bookreading_id)->exists()) {
                return ($new_bookreading_id);
            } else {
                return ('not added');
            }
        }

        if ($request->action == 'edit_bookreading') {

            if (Bookreading::where('id', $request->bookreading_id)->update([
                'day_of_week' => $request->day_of_week,
                'start_hour' => $request->start_hour,
                'start_minute' => $request->start_minute,
                'end_hour' => $request->end_hour,
                'end_minute' => $request->end_minute
            ])
            ) {
                return 'edited';
            } else {
                return 'not edited';
            }
        }

        if ($request->action == 'delete_bookreading') {

            if (Bookreading::where('id', $request->bookreading_id)->delete()) {
                return 'deleted';
            } else {
                return 'not deleted';
            }

        }
    }
}
