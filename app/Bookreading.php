<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookreading extends Model
{
    protected $fillable = ['user_id', 'book_id', 'day_of_week', 'start_hour', 'start_minute', 'end_hour', 'end_minute'];
    public function book()
    {
        return $this->belongsTo(Book::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
