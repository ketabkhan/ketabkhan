<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'user_id', 'book_title', 'book_color'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bookreadings()
    {
        return $this->hasMany(Bookreading::class);
    }
}
