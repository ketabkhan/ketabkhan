@extends('userdash.layouts.default')

@section('page-title', 'پیشخوان')
@section('page-desc', 'نمایی کلی از کارهایتان!')

@section('menu_home_status', 'active')

@section('custom-css')
    <link href="{{ url('assets/plugins/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet"/>
    <link href="{{ url('assets/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('custom-js')

    {{--<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>--}}
    {{--<script src="assets/plugins/select2/select2.min.js" type="text/javascript"></script>--}}

@stop

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-header  header-title">کتاب‌خوانی</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-header  header-title">درس‌خوانی</h4>
            <p>به‌زودی...</p>

        </div>
    </div>

@stop