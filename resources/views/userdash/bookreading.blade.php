<?php
function classForBookColor($color)
{
    if ($color == 'teal')
        return 'bg-custom';
    elseif ($color == 'white')
        return 'bg-white';
    elseif ($color == 'blue')
        return 'bg-primary';
    elseif ($color == 'green')
        return 'bg-success';
    elseif ($color == 'light-blue')
        return 'bg-info';
    elseif ($color == 'orange')
        return 'bg-warning';
    elseif ($color == 'red')
        return 'bg-danger';
    elseif ($color == 'black')
        return 'bg-inverse';
    elseif ($color == 'purple')
        return 'bg-purple';
    elseif ($color == 'pink')
        return 'bg-pink';
}

/**
 * @author Mohammad Mahdi Naderi
 * @param String $day
 * @return DateTime
 */
function getDateFromWeekDay($day)
{
    $days = ['Monday' => 1, 'Tuesday' => 2, 'Wednesday' => 3, 'Thursday' => 4, 'Friday' => 5, 'Saturday' => 6, 'Sunday' => 7];
    $today = new DateTime();
    $today->setTimezone(new DateTimeZone('Asia/Tehran'));


    if ($day == 'monday' OR $day == 'tuesday' OR $day == 'wednesday' OR $day == 'thursday' OR $day == 'friday') {

        $today->setISODate((int)$today->format('o'), (int)$today->format('W'), $days[ucfirst($day)]);
        return $today->format('Y-m-d');

    } elseif ($day == 'sunday' OR $day == 'saturday') {

        $today->setISODate((int)$today->format('o'), ((int)$today->format('W')) - 1, $days[ucfirst($day)]);
        return $today->format('Y-m-d');

    }
}
?>

@extends('userdash.layouts.default')

@section('page-title','کتاب‌خوانی')
@section('page-desc', 'اینجا می‌توانید به‌راحتی برای خواندن کتاب برنامه‌ریزی کنید.')

@section('menu_ketabkhani_status', 'active')

@section('custom-css')
    <link href="{{ url('assets/plugins/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet"/>
    <link href="{{ url('assets/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/pages.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-3" id="myAffixWrapper">
                    <nav style="top:80px;" id="myAffix">
                        <div class="widget">
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <a href="#" data-toggle="modal" data-target="#add-category"
                                           class="btn btn-lg btn-default btn-block waves-effect waves-light">
                                            <i class="fa fa-plus"></i> افزودن کتاب
                                        </a>
                                        <div id="external-events" class="m-t-20">
                                            <br/>
                                            <h4>کتاب‌ها <span data-toggle="tooltip" data-placement="left"
                                                              title="برای مشخص کردن ساعات مطالعه‌ی یک کتاب، کتاب را بگیرید و درون تقویم بیندازید."><i
                                                            class="md md-info-outline"></i></span></h4>
                                            <div>

                                                @foreach($user_books as $item)
                                                    <div class="external-event {{ classForBookColor($item->book_color) }}"
                                                         data-class="{{ classForBookColor($item->book_color) }}"
                                                         data-bookid="{{ $item->id }}"
                                                         data-booktitle="{{ $item->book_title }}"
                                                         style="position: relative;">
                                                        {{ $item->book_title  }}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>

                </div> <!-- end col-->
                <div class="col-md-9">
                    <div class="card-box" id="calendar-card-box">
                        <div id="calendar"></div>
                    </div>
                </div> <!-- end col -->
            </div>  <!-- end row -->

            <!-- BEGIN MODAL -->
            <div class="modal fade none-border" id="event-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><strong>زمان مطالعه‌ی کتاب</strong></h4>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">بستن</button>
                            <button type="button" class="btn btn-danger delete-event waves-effect waves-light"
                                    data-dismiss="modal">حذف‌کردن این زمان مطالعه
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Add Category -->
            <div class="modal fade none-border" id="add-category">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">افزودن کتاب</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">نام کتاب</label>
                                        <input class="form-control form-white" placeholder="نام کتاب را وارد کنید..."
                                               type="text"
                                               name="category-name"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">انتخاب یک رنگ برای کتاب</label>
                                        <select class="form-control form-white" data-placeholder="یک رنگ انتخاب کنید..."
                                                name="category-color">
                                            <option value="success">سبز</option>
                                            <option value="danger">قرمز</option>
                                            <option value="primary">آبی</option>
                                            <option value="info">آبی روشن</option>
                                            <option value="custom">سبزآبی</option>
                                            <option value="pink">صورتی</option>
                                            <option value="warning">نارنجی</option>
                                            <option value="inverse">سورمه‌ای</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">انصراف
                            </button>
                            <button type="button" class="btn btn-success waves-effect waves-light save-category"
                                    data-dismiss="modal">افزودن
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
        <!-- end col-12 -->
    </div> <!-- end row -->
@stop


@section('custom-js')
    <script src="{{ url('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ url('assets/plugins/select2/select2.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ url('assets/plugins/fullcalendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ url('assets/plugins/fullcalendar/dist/fa.js') }}"></script>
    <script src="{{ url('assets/plugins/notifyjs/dist/notify.min.js') }}"></script>
    <script src="{{ url('assets/plugins/notifications/notify-metro.js') }}"></script>

    <script>
        var modifyUrl = '{{ url('dashboard/bookreading/modify') }}';
        var defaultEvents = [
                @foreach($user_bookreadings as $item)
            {
                id: '{{ $item->id }}',
                title: '{{ $item->book->book_title }}',
                start: '{{ getDateFromWeekDay($item->day_of_week) }}T{{ $item->start_hour }}:{{ $item->start_minute }}:00',
                end: '{{ getDateFromWeekDay($item->day_of_week) }}T{{ $item->end_hour }}:{{ $item->end_minute }}:00',
                className: '{{ classForBookColor($item->book->book_color) }}'
            },
            @endforeach
        ];
    </script>

    {{-- Must be after define 'defaultEvents' --}}
    <script src="{{ url('assets/pages/ketabkhani.fullcalendar.js') }}"></script>

    <script>
        if (Modernizr.mq('(min-width: 990px)')) {
            $('#myAffix').affix({
                offset: {
                    top: 100,
                    bottom: 40
                }
            });
            $('#myAffix').width($("#myAffixWrapper").width());
        }
    </script>
@stop