<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>کتاب‌خوان | @yield('page-title')</title>

    <link rel="stylesheet" href="{{ url('assets/plugins/morris/morris.css') }}">

    <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/bootstrap-rtl.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/core.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/components.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/nprogress.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/plugins/animate.less/animate.min.css') }}" rel="stylesheet" type="text/css"/>



@yield('custom-css')

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{ url('assets/js/modernizr.min.js') }}"></script>

</head>

<?php
if(isset($menu_status_change) && $menu_status_change == 'folded') {
    $body_class_for_menu = 'fixed-left-void';
    $wrapper_class_for_menu = 'enlarged forced';
}
elseif(isset($menu_status_change) && $menu_status_change == 'expanded')
$body_class_for_menu = 'fixed-left';
elseif(Auth::guard('user')->user()->dash_menu_status == 'folded') {
    $body_class_for_menu = 'fixed-left-void';
    $wrapper_class_for_menu = 'enlarged forced';
}
elseif(Auth::guard('user')->user()->dash_menu_status == 'expanded')
    $body_class_for_menu = 'fixed-left';
?>

<body class="widescreen {{ $body_class_for_menu }}">

<div class="animationload">
    <div class="loader"></div>
</div>

<!-- Begin page -->
<div id="wrapper" class="{{ isset($wrapper_class_for_menu) ? $wrapper_class_for_menu : '' }}">

    <!-- Top Bar Start -->
    <div class="topbar" wrapper-class="{{ $body_class_for_menu }}">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="{{ url('dashboard') }}" class="logo">
                    <div class="icon-c-logo"><img width="30px" height="30px" src="{{ url('assets/images/khaanesh-logo.svg') }}"/></div>
                    <span><img width="30px" height="30px" src="{{ url('assets/images/khaanesh-logo.svg') }}"/>  کتاب‌خوان</span>
                </a>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-right">

                        <a href="{{ url('dashboard/?action=change_menu_status') }}" class="button-menu-mobile" id="menu-button">
                            <i class="ion-navicon"></i>
                        </a>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-left">
                        {{--<li class="dropdown hidden-xs">
                            <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light"
                               data-toggle="dropdown" aria-expanded="true">
                                <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-lg">
                                <li class="notifi-title"><span class="label label-default pull-left">3 جدید</span>اعلانات
                                </li>
                                <li class="list-group nicescroll notification-list">
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-right p-r-10">
                                                <em class="fa fa-diamond fa-2x text-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">یک نظم جدید دارای یک نظم جدید قرار داده شده
                                                    است قرار داده شده است</h5>
                                                <p class="m-0">
                                                    <small>تنظیمات جدید در دسترس وجود دارد</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-right p-r-10">
                                                <em class="fa fa-cog fa-2x text-custom"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">تنظیمات جدید</h5>
                                                <p class="m-0">
                                                    <small>تنظیمات جدید در دسترس وجود دارد</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="list-group-item text-right">
                                        <small class="font-600">دیدن همه اطلاعیه ها</small>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                        </li>--}}
                        <li class="beta hidden-xs">
                            <img class="animated infinite jello" data-toggle="tooltip" data-placement="bottom" title="ورژن ۰.۹" src="{{ url('assets/images/beta.png') }}" />
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img id="topbar-avatar" src="{{ url("assets/images/".Auth::guard('user')->user()->type."-student.png") }}" alt="user-img" class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('dashboard/profile') }}"><i class="ti-user m-r-5"></i> پروفایل</a></li>
                                {{--<li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> تنظیمات</a></li>--}}
                                <li><a href="{{ url('logout') }}"><i class="ti-power-off m-r-5"></i> خروج</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details">
                <div class="pull-right">
                    <img src="{{ url("assets/images/".Auth::guard('user')->user()->type."-student.png") }}" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false">{{ Auth::guard('user')->user()->name }}</a>
                    </div>
                    <p class="text-muted m-0">
                        {{ Auth::guard('user')->user()->type == 'seminary' ? 'طلبه' : '' }}
                        {{ Auth::guard('user')->user()->type == 'college' ? 'دانشجو' : '' }}
                        {{ Auth::guard('user')->user()->type == 'school' ? 'دانش‌آموز' : '' }}
                        {{ Auth::guard('user')->user()->type == 'normal' ? 'کتاب‌خوان' : '' }}
                    </p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>

                    <li>
                        <a href="{{ url('dashboard') }}" class="waves-effect @yield('menu_home_status')"><i class="ti-home"></i> <span>پیشخوان</span> </a>
                    </li>

                    @if(Auth::guard('user')->user()->type != 'normal')
                    <li>
                        <a href="#" class="waves-effect @yield('menu_darskhani_status')"><i class="ti-briefcase"></i> <span>درس‌خوانی (در آینده)</span></a>
                    </li>
                    @endif

                    <li>
                        <a href="{{ url('dashboard/bookreading') }}" class="waves-effect @yield('menu_ketabkhani_status')"><i class="ti-book"></i> <span>کتاب‌خوانی</span> </a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@yield('page-title')</h4>
                        <p class="text-muted page-title-alt">@yield('page-desc')</p>
                    </div>
                </div>


                @yield('content')


            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            <ul class="list-inline">
                <li><a href="#">خانه</a></li>
                <li><a href="#">بلاگ</a></li>
                <li><a href="#">تماس با ما</a></li>
            </ul>
            <span class="pull-left"> ۱۳۹۵ © تمامی حقوق برای «کتاب‌خوان» محفوظ است.</span>
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- jQuery  -->
<script src="{{ url('assets/js/jquery.min.js') }}"></script>
<script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/js/detect.js') }}"></script>
<script src="{{ url('assets/js/fastclick.js') }}"></script>

<script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ url('assets/js/waves.js') }}"></script>
<script src="{{ url('assets/js/wow.min.js') }}"></script>
<script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>

<script src="{{ url('assets/js/jquery.core.js') }}"></script>
<script src="{{ url('assets/js/jquery.app.js') }}"></script>
<script src="{{ url('assets/js/jquery.pjax.js') }}"></script>
<script src="{{ url('assets/js/nprogress.js') }}"></script>

@yield('custom-js')

<script>
    var resizefunc = [];
</script>

<script>
    $(document).pjax("a.pjax", '#wrapper');
    $(document).on('pjax:start', function() {
        NProgress.start();
    });

    $(document).on('pjax:end',   function() {
        NProgress.done();

        // does current browser support PJAX
        if ($.support.pjax) {
            $.pjax.defaults.timeout = 1000; // time in milliseconds
        }
    });
</script>

<script type="text/javascript">
    if (Modernizr.mq('(min-width: 990px)')) {
        var clickyab_ad = clickyab_ad || [];
        clickyab_ad['id'] = 5981466116337;
        clickyab_ad['domain'] = 'ketabkhanplus.ir';
        clickyab_ad['slot'] = 42271264861;
        clickyab_ad['width'] = 120;
        clickyab_ad['height'] = 240;
    }
</script>
<script type="text/javascript" src="http://a.clickyab.com/show.js"></script>

</body>
</html>