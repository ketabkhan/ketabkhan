<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

    <title>خوانش | @yield('page-title')</title>

    <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/core.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../assets/js/modernizr.min.js"></script>

</head>

<body class="widescreen @if(Auth::guard('admin')->user()->dash_menu_status == 'folded')fixed-left-void @else fixed-left @endif">

<div class="animationload">
    <div class="loader"></div>
</div>

<!-- Begin page -->
<div id="wrapper" @if(Auth::guard('admin')->user()->dash_menu_status == 'folded')class="enlarged forced" @endif>

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="{{ url('admin') }}" class="logo">
                    <div class="icon-c-logo"><img width="30px" height="30px" src="../assets/images/khaanesh-logo.svg"/></div>
                    <span><img width="30px" height="30px" src="../assets/images/khaanesh-logo.svg"/> خوانش</span>
                </a>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-right">
                        <a href="#" class="button-menu-mobile">
                            <i class="ion-navicon"></i>
                        </a>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-left">
                        <li class="dropdown hidden-xs">
                            <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light"
                               data-toggle="dropdown" aria-expanded="true">
                                <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-lg">
                                <li class="notifi-title"><span class="label label-default pull-left">3 جدید</span>اعلانات
                                </li>
                                <li class="list-group nicescroll notification-list">
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-right p-r-10">
                                                <em class="fa fa-diamond fa-2x text-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">یک نظم جدید دارای یک نظم جدید قرار داده شده
                                                    است قرار داده شده است</h5>
                                                <p class="m-0">
                                                    <small>تنظیمات جدید در دسترس وجود دارد</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-right p-r-10">
                                                <em class="fa fa-cog fa-2x text-custom"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">تنظیمات جدید</h5>
                                                <p class="m-0">
                                                    <small>تنظیمات جدید در دسترس وجود دارد</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="list-group-item text-right">
                                        <small class="font-600">دیدن همه اطلاعیه ها</small>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img id="topbar-avatar" src="../assets/images/default-avatar.svg" alt="user-img" class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> پروفایل</a></li>
                                <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> تنظیمات</a></li>
                                <li><a href="{{ url('admin/logout') }}"><i class="ti-power-off m-r-5"></i> خروج</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details">
                <div class="pull-right">
                    <img src="../assets/images/default-avatar.svg" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false">{{ Auth::guard('admin')->user()->name }}</a>
                    </div>
                    <p class="text-muted m-0">مدیر</p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>

                    <li class="text-muted menu-title">Navigation</li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect active"><i class="ti-home"></i> <span> داشبورد </span> </a>
                        <ul class="list-unstyled">
                            <li class="active"><a href="{{ url('admin') }}">پیشخوان</a></li>
                            <li><a href="{{ url('admin/register') }}">افزودن مدیر</a></li>
                            <li><a href="dashboard_3.html">داشبورد 3</a></li>
                        </ul>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@yield('page-title')</h4>
                        <p class="text-muted page-title-alt">@yield('page-desc')</p>
                    </div>
                </div>


                @yield('content')


            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            2015 © Ubold.
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- jQuery  -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/detect.js"></script>
<script src="../assets/js/fastclick.js"></script>

<script src="../assets/js/jquery.slimscroll.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script src="../assets/js/waves.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>

<script src="../assets/js/jquery.core.js"></script>
<script src="../assets/js/jquery.app.js"></script>

<script>
    var resizefunc = [];
</script>

<script>

</script>

</body>
</html>