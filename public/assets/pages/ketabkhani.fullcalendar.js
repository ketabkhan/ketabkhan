!function ($) {
    "use strict";

    var CalendarApp = function () {
        this.$body = $("body")
        this.$modal = $('#event-modal'),
            this.$event = ('#external-events div.external-event'),
            this.$calendar = $('#calendar'),
            this.$saveCategoryBtn = $('.save-category'),
            this.$categoryForm = $('#add-category form'),
            this.$extEvents = $('#external-events'),
            this.$calendarObj = null
    };


    /* on drop */
    CalendarApp.prototype.onDrop = function (eventObj, date) {

        var $this = this;
        var $bookId = eventObj.attr('data-bookid');
        var $bookTitle = eventObj.attr('data-booktitle');

        var endtime = new moment(date);
        endtime.add(1, 'h');

        $.ajax({
            url: modifyUrl,
            data: {
                action: 'add_bookreading',
                book_id: $bookId.toString(),
                day_of_week: date.lang('en').format('dddd').toLowerCase(),
                start_hour: date.lang('en').format('HH'),
                start_minute: date.lang('en').format('mm'),
                end_hour: endtime.lang('en').format('HH')
            },
            type: "GET",
            dataType: "text",
            beforeSend: function (result) {
                $(".loader").delay(100).fadeIn();
                $(".animationload").delay(100).fadeIn("slow");
            }
        })
            .done(function (data) {
                if (!isNaN(data)) {

//                                alert('added');
                    // eventObj = hamooni ke drag drop mikonimesh

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = eventObj.data('eventObject');
                    var $bookClass = eventObj.attr('data-class');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);
                    copiedEventObject.id = data;
                    // assign it the date that was reported
                    copiedEventObject.start = date.lang('en');
                    copiedEventObject.end = endtime.lang('en');


                    if ($bookClass)
                        copiedEventObject['className'] = [$bookClass];
                    // render the event on the calendar
                    $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);

//                                var newEvent = new Object();
//
//                                newEvent.title = "some text";
//                                newEvent.start = new Date();
//                                newEvent.allDay = false;
//                                $('#calendar').fullCalendar( 'renderEvent', newEvent );

                    $.Notification.notify('success', 'bottom left', 'اضافه شد!', 'زمان مطالعه برای کتاب «' + $bookTitle + '» ثبت شد.');

                } else {
                    $.Notification.notify('error', 'bottom left', 'اضافه نشد!', 'مشکلی در اضافه‌کردن زمان مطالعه هست.');
                }
            })
            .fail(function (xhr, status, errorThrown) {
                $.Notification.notify('error', 'bottom left', 'اضافه نشد!', 'مشکلی در اضافه‌کردن زمان مطالعه هست.');
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
            })
            .always(function (xhr, status) {
//                            alert("The request is complete!");
                $(".loader").delay(100).fadeOut();
                $(".animationload").delay(100).fadeOut("slow");
            });

    },

        CalendarApp.prototype.onResize = function (eventObj, delta, revertFunc, jsEvent, ui, view, testvar) {

            $.ajax({
                url: modifyUrl,
                data: {
                    action: 'edit_bookreading',
                    bookreading_id: eventObj.id,
                    day_of_week: eventObj.start.lang('en').format('dddd').toLowerCase(),
                    start_hour: eventObj.start.lang('en').format('HH'),
                    start_minute: eventObj.start.lang('en').format('mm'),
                    end_hour: eventObj.end.lang('en').format('HH'),
                    end_minute: eventObj.end.lang('en').format('mm')
                },
                type: "GET",
                dataType: "text",
                beforeSend: function (result) {
                    $(".loader").delay(100).fadeIn();
                    $(".animationload").delay(100).fadeIn("slow");
                }
            })
                .done(function (data) {
                    if (data == 'edited') {
                        $.Notification.notify('success', 'bottom left', 'تغییر یافت!', 'زمان مطالعه برای کتاب «' + eventObj.title + '» تغییر یافت.');
                    } else {
                        $.Notification.notify('error', 'bottom left', 'تغییر نیافت!', 'مشکلی در تغییر زمان مطالعه هست.');
                    }
                })
                .fail(function (xhr, status, errorThrown) {
                    $.Notification.notify('error', 'bottom left', 'تغییر نیافت!', 'مشکلی در تغییر زمان مطالعه هست.');
                    console.log("Error: " + errorThrown);
                    console.log("Status: " + status);
                    console.dir(xhr);
                })
                .always(function (xhr, status) {
                    $(".loader").delay(100).fadeOut();
                    $(".animationload").delay(100).fadeOut("slow");
                });

        },

        /* on click on event */
        CalendarApp.prototype.onEventClick = function (calEvent, jsEvent, view) {
            var $this = this;
            var form = $("<form></form>");
            form.append("زمان: " + calEvent.start.format('dddd') + '‌ها، از ' + calEvent.start.format('HH:mm') + ' تا ' + calEvent.end.format('HH:mm'));
            $this.$modal.modal({
                backdrop: 'static'
            });
            $this.$modal.find('.delete-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.delete-event').unbind('click').click(function () {

                $.ajax({
                    url: modifyUrl,
                    data: {
                        action: 'delete_bookreading',
                        bookreading_id: calEvent.id
                    },
                    type: "GET",
                    dataType: "text",
                    beforeSend: function (result) {
                        $(".loader").delay(100).fadeIn();
                        $(".animationload").delay(100).fadeIn("slow");
                    }
                })
                    .done(function (data) {
                        if (data == 'deleted') {
                            $this.$calendarObj.fullCalendar('removeEvents', function (ev) {
                                return (ev._id == calEvent._id);
                            });
                            $.Notification.notify('success', 'bottom left', 'حذف شد!', 'زمان مطالعه برای کتاب «' + calEvent.title + '» حذف شد.');
                        } else {
                            $.Notification.notify('error', 'bottom left', 'حذف نشد!', 'مشکلی در حذف زمان مطالعه هست.');
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        $.Notification.notify('error', 'bottom left', 'حذف نشد!', 'مشکلی در حذف زمان مطالعه هست.');
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    })
                    .always(function (xhr, status) {
                        $(".loader").delay(100).fadeOut();
                        $(".animationload").delay(100).fadeOut("slow");
                        $this.$modal.modal('hide');
                    });
            });

        },

        /* on select */
        /*CalendarApp.prototype.onSelect = function (start, end, allDay) {
         var $this = this;
         $this.$modal.modal({
         backdrop: 'static'
         });
         var form = $("<form></form>");
         form.append("<div class='row'></div>");
         form.find(".row")
         .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Event Name</label><input class='form-control' placeholder='Insert Event Name' type='text' name='title'/></div></div>")
         .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Category</label><select class='form-control' name='category'></select></div></div>")
         .find("select[name='category']")
         .append("<option value='bg-danger'>Danger</option>")
         .append("<option value='bg-success'>Success</option>")
         .append("<option value='bg-purple'>Purple</option>")
         .append("<option value='bg-primary'>Primary</option>")
         .append("<option value='bg-pink'>Pink</option>")
         .append("<option value='bg-info'>Info</option>")
         .append("<option value='bg-warning'>Warning</option></div></div>");
         $this.$modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
         form.submit();
         });
         $this.$modal.find('form').on('submit', function () {
         var title = form.find("input[name='title']").val();
         var beginning = form.find("input[name='beginning']").val();
         var ending = form.find("input[name='ending']").val();
         var categoryClass = form.find("select[name='category'] option:checked").val();
         if (title !== null && title.length != 0) {
         $this.$calendarObj.fullCalendar('renderEvent', {
         title: title,
         start: start,
         end: end,
         allDay: false,
         className: categoryClass
         }, true);
         $this.$modal.modal('hide');
         }
         else {
         alert('You have to give a title to your event');
         }
         return false;

         });
         $this.$calendarObj.fullCalendar('unselect');
         },*/

        CalendarApp.prototype.enableDrag = function () {
            //init events
            $(this.$event).each(function () {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });
        }

    /* Initializing */
    CalendarApp.prototype.init = function () {
        this.enableDrag();
        /*  Initialize the calendar  */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var today = new Date($.now());


        var $this = this;
        $this.$calendarObj = $this.$calendar.fullCalendar({
            slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
            slotLabelFormat: 'H:mm',
            /*minTime: '08:00:00',
             maxTime: '19:00:00',*/
            defaultView: 'agendaWeek',
            handleWindowResize: true,
            height: 'auto',
            header: {
                left: '',
                center: '',
                right: ''
            },
            nowIndicator: true,
            events: defaultEvents,
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events
            selectable: true,
            isJalaali: true,
            columnFormat: 'ddd',
            allDaySlot: false,
            isRTL: true,
            defaultTimedEventDuration: '01:00:00',
            eventOverlap: false,
            drop: function (date) {
                $this.onDrop($(this), date);
            },
            select: function (start, end, allDay) {
                $this.onSelect(start, end, allDay);
            },
            eventClick: function (calEvent, jsEvent, view) {
                $this.onEventClick(calEvent, jsEvent, view);
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                var testvar;
                testvar = $(event.target).attr('data-bookid');
                $this.onResize(event, delta, revertFunc, jsEvent, ui, view, testvar);
            },
            eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                var testvar;
                testvar = $(event.target).attr('data-bookid');
                $this.onResize(event, delta, revertFunc, jsEvent, ui, view, testvar);
            }

        });

        //on new event
        this.$saveCategoryBtn.on('click', function () {

            var book_title = $this.$categoryForm.find("input[name='category-name']").val();
            var book_color = $this.$categoryForm.find("select[name='category-color']").val();

            $.ajax({
                url: modifyUrl,
                data: {
                    action: 'add_book',
                    book_title: book_title,
                    book_color: book_color
                },
                type: "GET",
                dataType: "text",
                beforeSend: function (result) {
                    $(".loader").delay(100).fadeIn();
                    $(".animationload").delay(100).fadeIn("slow");
                }
            })
                .done(function (data) {
                    if (!isNaN(data)) {

                        if (book_title !== null && book_title.length != 0) {
                            $this.$extEvents.append('<div class="external-event bg-' + book_color + '" data-class="bg-' + book_color + '" data-bookid="' + data + '" data-booktitle="' + book_title + '" style="position: relative;"><i class="fa fa-move"></i>' + book_title + '</div>')
                            $this.enableDrag();

                            $.Notification.notify('success', 'bottom left', 'کتاب ' + book_title + ' اضافه شد!');
                        }
                    } else {
                        $.Notification.notify('error', 'bottom left', 'مشکلی در اضافه‌کردن کتاب ' + book_title + ' پیش آمد!');
                    }

                })
                .fail(function (xhr, status, errorThrown) {
                    $.Notification.notify('error', 'bottom left', 'مشکلی در اضافه‌کردن کتاب ' + book_title + ' پیش آمد!');
                    console.log("Error: " + errorThrown);
                    console.log("Status: " + status);
                    console.dir(xhr);
                })
                .always(function (xhr, status) {
                    $this.$categoryForm.find("input[name='category-name']").val('');
                    $(".loader").delay(100).fadeOut();
                    $(".animationload").delay(100).fadeOut("slow");
                });

        });
    },

        //init CalendarApp
        $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp

}(window.jQuery),

//initializing CalendarApp
    function ($) {
        "use strict";
        $.CalendarApp.init()
    }(window.jQuery);