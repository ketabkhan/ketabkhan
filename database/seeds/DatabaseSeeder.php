<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('admins')->insert([
            'id' => 1,
            'name' => 'محمّد مهدی نادری',
            'email' => 'mmnaderi.ir@gmail.com',
            'password' => bcrypt('29b768i71'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'جلال الدین',
            'email' => 'jalal@example.com',
            'password' => bcrypt('123456'),
            'type' => 'seminary',
            'dash_menu_status' => 'expanded',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('books')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'book_title' => 'قرآن',
                'book_color' => 'teal',
            ],
            [
                'id' => 2,
                'user_id' => 1,
                'book_title' => 'نهج البلاغه',
                'book_color' => 'blue',
            ]
        ]);

        DB::table('bookreadings')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'book_id' => 1,
                'day_of_week' => 'sunday',
                'start_hour' => '05',
                'start_minute' => '00',
                'end_hour' => '05',
                'end_minute' => '30',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 2,
                'user_id' => 1,
                'book_id' => 2,
                'day_of_week' => 'monday',
                'start_hour' => '16',
                'start_minute' => '00',
                'end_hour' => '16',
                'end_minute' => '30',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);

    }
}
